package org.example.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/testContext.xml")
@Transactional

public class FeatureTest {

    @Autowired
    private SessionFactory sessionFactory;
    private Session currentSession;

    @Before
    public void openSession() {
        currentSession = sessionFactory.getCurrentSession();
    }

    @Test
    public void shouldHaveASessionFactory() {
        assertNotNull(sessionFactory);
    }

    @Test
    public void shouldHaveNoObjectsAtStart() {
        List<?> results = currentSession.createQuery("from Feature").list();
        assertTrue(results.isEmpty());
    }
    
    @Test
    public void shouldBeAbleToPersistNumericalFeatureObject() {
        assertEquals(0, currentSession.createQuery("from Feature").list().size());
        
        Feature jobNumericalFeature = new Feature(0, "12", FeatureDataType.NUMERICAL);
        currentSession.persist(jobNumericalFeature);
        currentSession.flush();
        
        assertEquals(1, currentSession.createQuery("from Feature").list().size());
    }
    
    @Test
    public void shouldBeABleToQueryForNumericalFeatureObjects() {
    	shouldBeAbleToPersistNumericalFeatureObject();

        assertEquals(1, currentSession.createQuery("from Feature where name = '12' and datatype = 'NUMERICAL'").list().size());
        assertEquals(0, currentSession.createQuery("from Feature where name = '13' and dataype = 'NUMERICAL'").list().size());
    }
    
    @Test
    public void shouldBeAbleToPersistStringFeatureObject() {
        assertEquals(0, currentSession.createQuery("from Feature").list().size());
        Feature jobStringFeature = new Feature(1, "Describing something", FeatureDataType.STRING);
        currentSession.persist(jobStringFeature);
        
        currentSession.flush();
        
        assertEquals(2, currentSession.createQuery("from Feature").list().size());
    }
    
    @Test
    public void shouldBeABleToQueryForStringFeatureObjects() {
    	shouldBeAbleToPersistStringFeatureObject();
        assertEquals(1, currentSession.createQuery("from Feature where name = 'Describing something' and dataype = 'STRING'").list().size());
        assertEquals(0, currentSession.createQuery("from Feature where name = 'Describing something else' and dataype = 'STRING'").list().size());
    }
}
