package org.example.models;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Feature")
public class Feature {
	
	@Id
    @GeneratedValue(strategy = AUTO)
    @Column
    private int id;
	
	@Column(length = 150, nullable = false, unique = true)
    private String name;
	
	@Enumerated(EnumType.STRING)
    private FeatureDataType dataType;
    
    public Feature(int id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FeatureDataType getDataType() {
        return dataType;
    }
}
