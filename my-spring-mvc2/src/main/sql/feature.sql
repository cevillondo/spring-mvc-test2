CREATE TABLE  `gowlook`.`Feature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL UNIQUE,
  `dataType` ENUM('STRING', 'NUMERICAL'),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;